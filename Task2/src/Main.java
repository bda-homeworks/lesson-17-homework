import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        LinkedList<Integer> numbers = new LinkedList<>();
        numbers.add(25);
        numbers.add(50);
        numbers.add(75);
        numbers.add(100);
        numbers.add(125);

        numbers.add(0, 5);

        for (int number : numbers) {
            System.out.println(number);
        }

    }
}