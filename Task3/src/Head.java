import java.util.*;

public class Head {
    public static void main(String[] args) {
        List<LinkedList<Integer>> lists = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        // Create the four linked lists
        for (int i = 0; i < 4; i++) {
            LinkedList<Integer> list = new LinkedList<>();
            System.out.print("Enter the number of elements for List " + (i + 1) + ": ");
            int numElements = scanner.nextInt();
            System.out.print("Enter the " + numElements + " elements for List " + (i + 1) + ": ");
            for (int j = 0; j < numElements; j++) {
                int element = scanner.nextInt();
                list.add(element);
            }
            lists.add(list);
        }

        // Compare the sizes of the linked lists
        for (int i = 0; i < 3; i++) {
            int size1 = lists.get(i).size();
            int size2 = lists.get(i + 1).size();
            String comparison = "=";
            if (size1 < size2) {
                comparison = "<";
            } else if (size1 > size2) {
                comparison = ">";
            }
            System.out.println("List " + (i + 1) + " " + comparison + " List " + (i + 2));
        }
    }
}
